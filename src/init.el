(require 'cl)
(defun add-tangled-name (backend)
  (let ((src-blocks (org-element-map (org-element-parse-buffer) 'src-block #'identity)))
    (setq src-blocks (nreverse src-blocks))
    (loop for src in src-blocks
          do
          (goto-char (org-element-property :begin src))
          (let ((tangled-name (org-element-property :name src)))
            (if (bound-and-true-p tangled-name)
                (insert (format "/<%s>/ =\n" tangled-name)))))))

(add-hook 'org-export-before-parsing-hook 'add-tangled-name)
