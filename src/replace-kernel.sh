setYes() {
    # sets kernel config option to yes.
    for option in "$@"
    do
        sed -i "s,^.*CONFIG_${option} .*$,CONFIG_${option}=y," contrib/linux/.config
    done
}
dhclient # get an IP if we don't have one already.
apt-get update
apt-get build-dep linux
VERSION=4.19.1; export VERSION
pushd contrib
wget -c https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$VERSION.tar.xz
tar xf linux-$VERSION.tar.xz
ln -s linux-$VERSION linux
cp /boot/config-$(uname -r) linux/.config
popd
# syzkaller basic options
setYes KCOV KCOV_INSTRUMENT_ALL KCOV_ENABLE_COMPARISONS DEBUG_FS \
  DEBUG_INFO KALLSYMS KALLSYMS_ALL

# syzkaller bug detection
setYes CONFIG_KASAN CONFIG_KASAN_INLINE

# syzkaller fault injection
setYes FAULT_INJECTION FAULT_INJECTION_DEBUG_FS FAILSLAB \
       FAIL_PAGE_ALLOC FAIL_MAKE_REQUEST FAIL_IO_TIMEOUT FAIL_FUTEX

# syzkaller debugging configs
setYes LOCKDEP PROVE_LOCKING DEBUG_ATOMIC_SLEEP PROVE_RCU DEBUG_VM \
       REFCOUNT_FULL FORTIFY_SOURCE HARDENED_USERCOPY \
       LOCKUP_DETECTOR SOFTLOCKUP_DETECTOR HARDLOCKUP_DETECTOR \
       BOOTPARAM_HARDLOCKUP_PANIC DETECT_HUNG_TASK WQ_WATCHDOG

time make -k -j 4 -C contrib/linux menuconfig
time make -k -j 4 -C contrib/linux kvmconfig
time make -k -j 4 -C contrib/linux # do not remove this line, it is necessary.
time make -k -j 4 -C contrib/linux modules
time make -k -j 4 -C contrib/linux install
time make -k -j 4 -C contrib/linux modules_install
pushd contrib/linux
update-initramfs -c -k $VERSION
update-grub
popd
