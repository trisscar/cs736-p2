package='libvirt'
mkdir -p contrib
if [ ! -d contrib/source-package ]
then
    git clone https://gitlab.com/nickdaly/source-package.git contrib/source-package
fi
pushd contrib
# download
sudo bash source-package/source-package.sh $package -y
# build
sudo bash source-package/source-package.sh $package -y
popd
